require recipes-bsp/u-boot/u-boot.inc

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=1707d6db1d42237583f50183a5651ecb \
                    file://README;beginline=1;endline=22;md5=78b195c11cb6ef63e6985140db7d7bab"


PR = "r1"
PV = "v2013.07+git${SRCPV}"

# This revision corresponds to the tag "v2013.07"
# We use the revision in order to avoid having to fetch it from the repo during parse
SRCREV = "62c175fbb8a0f9a926c88294ea9f7e88eb898f6c"

SRC_URI = "git://git.denx.de/u-boot.git;branch=master;protocol=git"

S = "${WORKDIR}/git"

FILESDIR = "${@os.path.dirname(d.getVar('FILE',1))}/u-boot-git/${MACHINE}"

PACKAGE_ARCH = "${MACHINE_ARCH}"

UBOOT_SRC_PATH = "/usr/src/u-boot"

FILES_${PN}-dev = "${UBOOT_SRC_PATH}"

do_install_append () {

    ubootsrcdir=${D}${UBOOT_SRC_PATH}

    install -d $ubootsrcdir

    #
    # Copy the entire source tree. In case an external build directory is
    # used, copy the build directory over first, then copy over the source
    # dir. This ensures the original Makefiles are used and not the
    # redirecting Makefiles in the build directory.

    cp -fR * $ubootsrcdir
    if [ "${S}" != "${B}" ]; then
        cp -fR ${S}/* $ubootsrcdir
    fi
    oe_runmake -C $ubootsrcdir distclean

}
