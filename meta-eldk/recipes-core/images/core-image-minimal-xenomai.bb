SUMMARY = "Minimal root file system image with xenomai support"
DESCRIPTION = "Root FS includes the following functionality: \
		xenomai tools \
		"
IMAGE_INSTALL = "packagegroup-core-boot xenomai ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE = "8192"

# remove not needed ipkg informations
ROOTFS_POSTPROCESS_COMMAND += "remove_packaging_data_files ; "
