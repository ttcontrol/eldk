require linux.inc

# Mark archs/machines that this kernel supports
DEFAULT_PREFERENCE = "-1"
DEFAULT_PREFERENCE_generic-armv4t = "1"
DEFAULT_PREFERENCE_generic-armv5te = "1"
DEFAULT_PREFERENCE_generic-armv6 = "1"
DEFAULT_PREFERENCE_generic-armv7a = "1"
DEFAULT_PREFERENCE_generic-armv7a-hf = "1"
DEFAULT_PREFERENCE_generic-mips = "1"
DEFAULT_PREFERENCE_generic-powerpc = "1"
DEFAULT_PREFERENCE_generic-powerpc-softfloat = "1"
DEFAULT_PREFERENCE_generic-powerpc-4xx = "1"
DEFAULT_PREFERENCE_generic-powerpc-4xx-softfloat = "1"
DEFAULT_PREFERENCE_generic-powerpc-e500v2 = "1"

PR = "r1"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

SRC_URI = "git://kernel.ubuntu.com/ubuntu/linux.git;protocol=git \
	   file://0001-ARM-video-mxs-Fix-mxsfb-misconfiguring-VDCTRL0.patch \
	   file://0002-iio-mxs-Remove-unused-struct-mxs_lradc_chan.patch \
	   file://0003-iio-mxs-Implement-support-for-touchscreen.patch \
	   file://0004-ARM-mxs-Enable-touchscreen-on-m28evk.patch \
	   file://defconfig"

LINUX_VERSION ?= "3.8"
#LINUX_VERSION_EXTENSION ?= "-custom"

# tag: v3.8.13.2 cdf1a431a5fd7f338209c58ddc6c415e4d9fe047
# tag: v3.8.13.4 d1baa1360260b5a01938674fc518109a4e5a148d
# tag: v3.8.13.5 88c40fc9401a458b7e08f510ff0b2e0a5b4b2d01
SRCREV="88c40fc9401a458b7e08f510ff0b2e0a5b4b2d01"

PV = "${LINUX_VERSION}+git${SRCPV}"

S = "${WORKDIR}/git"

COMPATIBLE_MACHINE = "generic-armv4t|generic-armv5te|generic-armv6|generic-armv7a|generic-armv7a-hf|generic-mips|generic-powerpc|generic-powerpc-softfloat|generic-powerpc-4xx|generic-powerpc-4xx-softfloat|generic-powerpc-e500v2"
