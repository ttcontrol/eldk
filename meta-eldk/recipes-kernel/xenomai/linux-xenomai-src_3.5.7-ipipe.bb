SUMMARY = "Xenomai is a real-time development framework cooperating with the Linux \
	    kernel, in order to provide a pervasive, interface-agnostic, hard \
	    real-time support to user-space applications, seamlessly integrated \
	    into the GNU/Linux environment. This scenario adds source of ipipe \
	    patched kernel to the image"

HOMEPAGE = "http://www.xenomai.org"
SECTION = "tools/realtime"

KERNEL_IPIPE_SRC_PATH = "/usr/src/kernel-ipipe"
XENOMAI_SRC_PATH = "../xenomai"

DEPENDS = "xenomai"

# BBCLASSEXTEND += " nativesdk "

PROVIDES = "kernel-ipipe-dev"

PACKAGES = "kernel-ipipe-dev"

inherit kernel-arch

XENOMAI_VER="2.6.2.1"
LINUX_VER="${@d.getVar('PV',1).split('-')[0]}"
LINUX_VER_SHORT="${@d.getVar('PV',1).split('.')[0]}.${@d.getVar('PV',1).split('.')[1]}"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

SRC_URI = " \
           ${KERNELORG_MIRROR}/linux/kernel/v3.x/linux-${LINUX_VER}.tar.bz2;name=linux \
	   http://download.gna.org/xenomai/stable/xenomai-${XENOMAI_VER}.tar.bz2;name=xenomai/ \
         "

SRC_URI[xenomai.md5sum] = "3dca1eca040486f8f165f5e340c7a25e"
SRC_URI[xenomai.sha256sum] = "93298af1263469098896ac0ce4f38066efe4d7dfbc4e6ed869a435ef0c9a1747"

SRC_URI[linux.md5sum] = "636d19c8686623776764227b6d3b911d"
SRC_URI[linux.sha256sum] = "5174c0ab054ccafafc620e806c3a0542cbeaef7248d5960d0bc5fea5006c177e"

S="${WORKDIR}/linux-${LINUX_VER}"

PR = "r0"

FILES_kernel-ipipe-dev = "${KERNEL_IPIPE_SRC_PATH}"

do_configure() {
	# proceed xenomai patching
	${WORKDIR}/xenomai-${XENOMAI_VER}/scripts/prepare-kernel.sh --arch=${ARCH} \
	    --adeos=${WORKDIR}/xenomai-${XENOMAI_VER}/ksrc/arch/${ARCH}/patches/*ipipe*${LINUX_VER_SHORT}*${ARCH}*.patch \
        --linux=${S}
}

do_compile() {
        # just skip it
        kerneldir=${D}${KERNEL_IPIPE_SRC_PATH}
}

do_install() {
        kerneldir=${D}${KERNEL_IPIPE_SRC_PATH}
        install -d $kerneldir

        cp -fR * $kerneldir

	cd $kerneldir
	# change xenomai soft links
	find . -regex ".*xenomai.*" -type l -printf "export pathto=\`echo %p | sed -e 's@[^/]*/@../@g' \`; \
		export linkfile=\`readlink %p | sed 's@.*xenomai-${XENOMAI_VER}@@'\`; \
		export linkto=\$(dirname \$(dirname \$pathto))/${XENOMAI_SRC_PATH}\$linkfile ; \
		rm %p; ln -s \$linkto %p \n" | sh
}
