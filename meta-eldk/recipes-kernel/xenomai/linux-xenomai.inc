require ../linux/linux.inc

LINUX_VER="${@d.getVar('PV',1).split('-')[0]}"
XENOMAI_VER="2.6.2.1"

XENOMAI_SRC_PATH = "../xenomai"

SRC_URI = "http://download.gna.org/xenomai/stable/xenomai-${XENOMAI_VER}.tar.bz2;name=xenomai"

DEPENDS_append = " xenomai"

S="${WORKDIR}/linux-${LINUX_VER}"

do_configure_prepend() {
	# proceed xenomai patching
	${WORKDIR}/xenomai-${XENOMAI_VER}/scripts/prepare-kernel.sh --arch=${ARCH} \
	    --adeos=${WORKDIR}/xenomai-${XENOMAI_VER}/ksrc/arch/${ARCH}/patches/*ipipe*${LINUX_VER}*${ARCH}*.patch \
        --linux=${S}
}

kernel_do_install_append() {
	cd $kerneldir
	# change xenomai soft links
	find . -regex ".*xenomai.*" -type l -printf "export pathto=\`echo %p | sed -e 's@[^/]*/@../@g' \`; \
		export linkfile=\`readlink %p | sed 's@.*xenomai-${XENOMAI_VER}@@'\`; \
		export linkto=\$(dirname \$(dirname \$pathto))/${XENOMAI_SRC_PATH}\$linkfile ; \
		rm %p; ln -s \$linkto %p \n" | sh
}

