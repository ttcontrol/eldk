XENOMAI_VER="2.6.2.1"

require linux-xenomai.inc

# Mark archs/machines that this kernel supports
DEFAULT_PREFERENCE = "-1"

PR = "r0"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

SRC_URI += " \
	   ${KERNELORG_MIRROR}/linux/kernel/v3.x/linux-${LINUX_VER}.tar.bz2;name=linux \
           file://defconfig \
	   "

SRC_URI[xenomai.md5sum] = "3dca1eca040486f8f165f5e340c7a25e"
SRC_URI[xenomai.sha256sum] = "93298af1263469098896ac0ce4f38066efe4d7dfbc4e6ed869a435ef0c9a1747"

SRC_URI[linux.md5sum] = "636d19c8686623776764227b6d3b911d"
SRC_URI[linux.sha256sum] = "5174c0ab054ccafafc620e806c3a0542cbeaef7248d5960d0bc5fea5006c177e"
