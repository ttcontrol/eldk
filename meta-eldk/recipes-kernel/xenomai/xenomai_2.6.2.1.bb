SUMMARY = "Xenomai is a real-time development framework cooperating with the Linux \
	    kernel, in order to provide a pervasive, interface-agnostic, hard \
	    real-time support to user-space applications, seamlessly integrated \
	    into the GNU/Linux environment."

HOMEPAGE = "http://www.xenomai.org"
SECTION = "tools/realtime"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
PR = "r0"

SRC_URI = "http://download.gna.org/xenomai/stable/xenomai-${PV}.tar.bz2 \
	   "
SRC_URI[md5sum] = "3dca1eca040486f8f165f5e340c7a25e"
SRC_URI[sha256sum] = "93298af1263469098896ac0ce4f38066efe4d7dfbc4e6ed869a435ef0c9a1747"

inherit autotools

XENOMAI_SRC_PATH = "/usr/src/xenomai"

TARGET_CC_ARCH += "${LDFLAGS}"

prefix_x = "${prefix}/xenomai"

FILES_${PN} += "/dev/* ${prefix_x}/bin/* ${prefix_x}/sbin/* \
		 ${prefix_x}/bin/regression/native/* \
		 ${prefix_x}/bin/regression/native+posix/* \
		 ${prefix_x}/bin/regression/posix/* \
		 ${prefix_x}/lib/*.so.* ${prefix_x}/lib/pkgconfig ${prefix_x}/lib/posix.wrappers"
FILES_${PN}-doc += "${prefix_x}/share/*"
FILES_${PN}-dev += "${prefix_x}/include/* ${prefix_x}/lib/*.la ${prefix_x}/lib/*.so ${XENOMAI_SRC_PATH}"
FILES_${PN}-staticdev += "${prefix_x}/include/* ${prefix_x}/lib/*.a"
FILES_${PN}-dbg += "${prefix_x}/lib/.debug/* ${prefix_x}/bin/.debug/* ${prefix_x}/sbin/.debug/* \
		 ${prefix_x}/bin/regression/native/.debug/* \
		 ${prefix_x}/bin/regression/native+posix/.debug/* \
		 ${prefix_x}/bin/regression/posix/.debug/* \
"

do_configure() {
	cd ${S}

	# install xenomai source first to the temp folder - original source code of xenomai
	xenomaitmpdir=${WORKDIR}/xensrc
	install -d $xenomaitmpdir
	cp -fR * $xenomaitmpdir

        ${S}/configure --build=${BUILD_SYS} --host=${HOST_SYS} --target=${TARGET_SYS}
}

do_compile () {
	cd ${S}
	make
}

do_install () {
	cd ${S}
	make DESTDIR=${D} install

	# install sources of xenomai from temp folder created at configure stage
	cd ${WORKDIR}/xensrc
	xenomaidir=${D}${XENOMAI_SRC_PATH}
	install -d $xenomaidir
	cp -fR * $xenomaidir
}

sysroot_stage_all_append() {
        sysroot_stage_dir ${D}${XENOMAI_SRC_PATH} ${SYSROOT_DESTDIR}${XENOMAI_SRC_PATH}
}
